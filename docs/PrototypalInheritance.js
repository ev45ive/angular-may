function Person(name){
	this.name = name
}
Person.prototype.sayHello = function (){
    console.log('Hi, I am '+this.name)
}

function Employee(name, salary){
	Person.apply(this,arguments)
	this.salary = salary
}
Employee.prototype = Object.create(Person.prototype)

Employee.prototype.work = function(){
	console.log("I want my "+this.salary)
}
