class Person {

    constructor(name) {
        this.name = name
    }

    sayHello() {
        console.log('Hi, I am ' + this.name)
    }
}



class Employee extends Person {

    constructor(name, salary) {
        super(name)
        this.salary = salary
    }

    work() {
        console.log("I want my " + this.salary)
    }
}