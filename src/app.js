
import styles from './app.css'

console.log(styles)

document.querySelector('h1').classList.add(styles.specials)

export class AwesomeApp {

  constructor() {
    this.version = '1.0.3'
  }

  run() {
    console.log('Version ' + this.version)
  }
}

export const OPTIONS = {}

export default AwesomeApp

// module.exports = AwesomeApp
