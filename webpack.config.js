var MiniCssExtractPlugin = require('mini-css-extract-plugin')
var UglifyJSPlugin = require('uglifyjs-webpack-plugin')
var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

// webpack-merge
// var webpack = require('webpack')
// wepbpack.compile(...)

module.exports = {
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',

        options: {
          presets: ['env']
        }
      },
      {
        test: /\.(scss|css)$/,
        use: [
          //   {
          //   loader: 'style-loader'
          // },
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules:true
            }
          }, {
            loader: 'sass-loader'
          }
        ]
      }
    ]
  },

  plugins: [
    // new UglifyJSPlugin(),
    new MiniCssExtractPlugin({
      filename: 'styles.css'
    }),
    new HtmlWebpackPlugin({
      template:'./src/index.html'
    })
  ],
  entry: './src/main',

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },

  mode: 'development'
}